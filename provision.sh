#!/bin/bash
# based on https://linuxacademy.com/howtoguides/posts/show/topic/14756-install-the-mean-stack-on-vagrant ,
# mainly because I have a headache and didn't want write it myself.

# Packages
NODE="nodejs"
BUILD_ESSENTIAL="build-essential"
MONGO="mongodb-org"
GIT="git"

# Prerequisites
GIT_INSTALLED=$(dpkg-query -W --showformat='${Status}\n' $GIT | grep "install ok installed")
echo "Checking for $GIT: $GIT_INSTALLED"
if [ "" == "$GIT_INSTALLED" ]; then
  apt-get update
  apt-get install -y $GIT
fi
echo "GIT INSTALLED"

# MongoDB
MONGO_INSTALLED=$(dpkg-query -W --showformat='${Status}\n' $MONGO | grep "install ok installed")
echo "Checking for $MONGO: $MONGO_INSTALLED"
if [ "" == "$MONGO_INSTALLED" ]; then
  # never mind that ----, here comes Mongo!
  # add real mongo repo to apt sources
  apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
  echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list
  apt-get update
  apt-get install -y mongodb-org
fi
echo "MONGODB JUST PAWN IN GAME OF LIFE"

# Node.js
NODE_INSTALLED=$(dpkg-query -W --showformat='${Status}\n' $NODE | grep "install ok installed")
echo "Checking for $NODE: $NODE_INSTALLED"
if [ "" == "$NODE_INSTALLED" ]; then
  # sure, I'll use curl to download from a random site, what could possibly go wrong?
  curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
  apt-get install -y build-essential nodejs
fi
echo "NODEJS INSTALLED"

# this starts where I have to do my own work

# nmap is easier to interpret than netstat. Sue me.
apt install -y nmap

# start mongo
service mongod start

# Mongo doesn't use authentication by default. Homey don't play dat.
mongo --eval "db.createUser({user:'admin', pwd:'&YGV5rdx', roles: [{role: 'userAdminAnyDatabase', db: 'admin'}]});"
service mongod restart

# start node

